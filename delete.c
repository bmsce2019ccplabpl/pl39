#include<stdio.h>
int main()
{
    int n;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter %d numbers\n",n);
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    
    int pos;
    printf("Enter the position from which the  number should be deleted\n",n);
    scanf("%d",&pos);
    
    for(int i=pos;i<n-1;i++)
    {
        a[i]=a[i+1];
    }

    printf("The new array is\n");
    for(int i=0;i<n-1;i++)
    {
        printf("%d \n",a[i]);
    }
    
    return 0;
}
