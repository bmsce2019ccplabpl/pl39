#include<stdio.h>
int input()
{
    int n;
    printf("Enter the value of n\n");
    scanf("%d",&n);
    return n;
}

int add(int a)
{
    int num[a];
    printf("Enter %d numbers\n",a);
    for(int i=1;i<=a;i++)
    {
        scanf("%d",&num[i]);
    }
    
    int sum=0;
    for(int i=1;i<=a;i++)
    {
        sum=sum+num[i];
    }
    return sum;
}

void display(int a)
{
    printf("The sum of the numbers is %d\n",a);
}

int main()
{
    int n=input();
    int sum=add(n);
    display(sum);
    return 0;
}
